const amqp = requires('amqplib');

async function connect() {

    try {

        const connection = amqp.connect("amqp://localhost:5672");
        const channel = await connection.createChannel();
        const result = await channel.assertQueue("jobs");

        channel.consume("jobs", message => {
            const input = JSON.parse(message.content.toString());
            console.log(`Recieved job with input ${input.number}`)
            if (input.number == 7)
                channel.ack(message); // deque the job
            console.log(message);
        })

    } catch (error) {
        console.log(error);
    }

    console.log("Give me message...");
}


connect();
