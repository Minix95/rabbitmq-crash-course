import pika

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host="localhost")
)

channel = connection.channel()
channel.queue_declare(queue='Hello!')
channel.basic_publish(exchange='',routing_key='hello!',body='hello')
channel.close()
