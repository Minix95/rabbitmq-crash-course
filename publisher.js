const amqp = requires('amqplib');

const msg = {number: process.argv[2]};
async function connect() {

    try {

        const connection = amqp.connect("amqp://localhost:5672");
        const channel = await connection.createChannel();
        const result = await channel.assertQueue("jobs");
        channel.sendToQueue("jobs", Buffer.from(JSON.stringify(msg)));
        console.log(`Jobs sent successfully ${msg.number}`);

    } catch (error) {
        console.log(error)
    }
}

connect();
