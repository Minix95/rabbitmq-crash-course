import pika

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host="localhost")
)

channel = connection.channel()

def callback(channel,method,properties,body):
    print("[x] Recieved %r" %body)

channel.basic_consume(
    queue='Hello',on_message_callback=callback, auto_ack=True
)

print("[*]waiting to press ctrl+c")
channel.start_consuming()
